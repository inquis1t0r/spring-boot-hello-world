package com.inquis1t0r.springboothelloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;


@SpringBootApplication
@Controller
@EnableAutoConfiguration
public class SpringBootHelloWorldApplication {

    @RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello World!";
    }

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringBootHelloWorldApplication.class, args);
	}
}
